<!--
SPDX-FileCopyrightText: 2021 Romain Vigier <contact AT romainvigier.fr>
SPDX-License-Identifier: CC-BY-SA-4.0
-->

# Changelog


## [2.0.1] - 2021-09-28

### Added
- New Basque translation (contributed by @sergitroll9)

### Changed
- Dependency upon libadwaita 1.0.0.alpha.3
- Updated Croatian translation (contributed by @milotype)
- Updated Dutch translation (contributed by @Vistaus)
- Updated French translation
- Updated German translation (contributed by @milotype)
- Updated Hungarian translation (contributed by @urbalazs)
- Updated Lithuanian translation (contributed by @completed)
- Updated Spanish translation (contributed by @oscfdezdz)
- Updated Swedish translation (contributed by @eson)
- Updated Turkish translation (contributed by @ersen)

### Fixed
- Writing style (contributed by @BrainBlasted)
- Typos (contributed by @urbalazs)


## [2.0.0] - 2021-09-22

Metadata Cleaner v2.0.0 is a major release featuring a brand new user interface written in GTK4 and using libadwaita, a new help system and a whole set of new and updated translations.

### Added
- Dependency upon GTK >=4.4
- Dependency upon libadwaita commit `03f159488ec3b8e3ea4c3c2daa9c408b071d512d` (as the library is still in development, it is subject to API changes. When it is released, a new version of Metadata Cleaner will be tagged.)
- Help pages trough Yelp
- Adaptive user interface
- Details about a finished cleaning
- Menu item for clearing the window
- Persistent lightweight cleaning preference
- Window size saved on close
- New Hungarian translation (contributed by @urbalazs)
- New Japanese translation (contributed by Kaede)
- New Occitan translation (contributed by @quenty_occitania)
- Initial work on Danish translation (contributed by @kingu)
- Initial work on Norwegian Nynorsk translation (contributed by @kingu)
- Initial work on Russian translation (contributed by @BigmenPixel)

### Changed
- Cleaning and saving merged into a single action
- Social links added in the About dialog
- Updated Croatian translation (contributed by @milotype)
- Updated Dutch translation (contributed by @Vistaus)
- Updated Finnish translation (contributed by @artnay)
- Updated French translation
- Updated German translation (contributed by @milotype)
- Updated Indonesian translation (contributed by @t7260 and Reza Almanda)
- Updated Italian translation (contributed by @albanobattistella)
- Updated Lithuanian translation (contributed by @completed)
- Updated Norwegian Bokmål translation (contributed by @kingu)
- Updated Portuguese (Brazil) translation (contributed by @rafaelff and @xfgusta)
- Updated Spanish translation (contributed by @oscfdezdz)
- Updated Swedish translation (contributed by @eson and @kingu)
- Updated Turkish translation (contributed by @ersen)

### Removed
- Dependency upon GTK3 and libhandy


## [1.0.8] - 2021-08-10

### Added
- New Dutch translation (contributed by @Vistaus)
- New Italian translation (contributed by @albanobattistella)
- Initial work on Sinhala translation (contributed by HelaBasa)

### Changed
- Updated Indonesian translation (contributed by @t7260)


## [1.0.7] - 2021-07-05

### Added
- New Lithuanian translation (contributed by Gediminas Murauskas)
- Initial work on Finnish translation (contributed by @artnay)
- Initial work on Indonesian translation (contributed by @kingu)
- Initial work on Norwegian Bokmål translation (contributed by @t7260 and Reza Almanda)


## [1.0.6] - 2021-05-04

### Added

- New Croatian translation (contributed by @milotype)
- New Portuguese (Brazil) translation (contributed by @xfgusta)


## [1.0.5] - 2021-04-13

### Fixed
- Wrong accelerator in the shortcuts window


## [1.0.4] - 2021-04-02

### Added
- New Turkish translation (contributed by @ersen)


## [1.0.3] - 2021-02-17

### Changed
- Updated Spanish translation


## [1.0.2] - 2021-01-15

### Added
- New Spanish translation (contributed by @oscfdezdz)
- New Swedish translation (contributed by @eson)

### Fixed
- Files with uppercase extension can't be added


## [1.0.1] - 2020-12-28

### Added
- New German translation (contributed by @lux)


## [1.0.0] - 2020-12-10

First release! 🎉
